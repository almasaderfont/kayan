jQuery(document).ready(function ($) {
  realEstateSwiperInit();
  lazyLoad();
  mixItUpInit();
});

// functions init

function lazyLoad() {
  const images = document.querySelectorAll(".lazy-omd");

  const optionsLazyLoad = {
    //  rootMargin: '-50px',
    // threshold: 1
  };

  const preloadImage = function (img) {
    img.src = img.getAttribute("data-src");
    img.onload = function () {
      img.parentElement.classList.remove("loading-omd");
      img.parentElement.classList.add("loaded-omd");
      img.parentElement.parentElement.classList.add("lazy-head-om");
    };
  };

  const imageObserver = new IntersectionObserver(function (enteries) {
    enteries.forEach(function (entery) {
      if (!entery.isIntersecting) {
        return;
      } else {
        preloadImage(entery.target);
        imageObserver.unobserve(entery.target);
      }
    });
  }, optionsLazyLoad);

  images.forEach(function (image) {
    imageObserver.observe(image);
  });
}

function swiperInit(options) {
  // console.log(options);
  const swiper = new Swiper(options.className + " .swiper-container", {
    spaceBetween: 30,
    autoplay: {
      delay: 3000,
      disableOnInteraction: false,
    },
    rtl: $("html").attr("dir") === "rtl" ? true : false,
    pagination: {
      el: options.className + " .swiper-pagination",
      clickable: true,
    },
    navigation: {
      nextEl: options.className + " .swiper-button-next",
      prevEl: options.className + " .swiper-button-prev",
    },
    breakpoints: options.breakpoints,
    observer: options.observer,
    observeParents: options.observeParents,
    grid: options.grid,
    ...options,
  });

  lazyLoad();

  return swiper;
}

function realEstateSwiperInit() {
  const realEstateSwiperBreakNormalPoints = {
    0: {
      slidesPerView: 1,
    },
    480: {
      slidesPerView: 2,
    },
    767: {
      slidesPerView: 3,
    },
    992: {
      slidesPerView: 3,
    },
    1200: {
      slidesPerView: 4,
    },
  };

  const realEstateSliderSectionProps = {
    className: ".slider_content_wrapper__",
    breakpoints: realEstateSwiperBreakNormalPoints,
    spaceBetween: 0,
    autoplay: false,
  };

  swiperInit(realEstateSliderSectionProps);
}

function mixItUpInit() {
  let specialtiesMixItUpElement = document.getElementById(
    "mix_content_wrapper__"
  );

  if (specialtiesMixItUpElement) {
    mixitup(specialtiesMixItUpElement, {
      selectors: {
        control: ".filter_control",
        target: ".filter_item",
      },
    });
  }

  $(".filter").each((index, element) => {
    if (index === 0) element.click();
  });
}
